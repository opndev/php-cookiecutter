#!/bin/sh

set -e

git init

git config commit.template .git-commit-template

HOOKS={{cookiecutter.use_hooks}}

if [ "$HOOKS" = "yes" ]
then
    git config core.hooksPath dev-bin/git-hooks
fi

git add -A
git commit --no-verify \
    -m "Initial import of php-{{cookiecutter.repo_name}}"

git remote add upstream git@gitlab.com:opndev/php-{{cookiecutter.repo_name}}
git remote add origin git@gitlab.com:{{cookiecutter.gitlab_username}}/php-{{cookiecutter.repo_name}}

exit 0
