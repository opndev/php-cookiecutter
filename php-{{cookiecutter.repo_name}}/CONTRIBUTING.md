# Contributing to the {{cookiecutter.repo_name}} project

Thank you for your support and effort in helping this project grow. We welcome
all contributors to our codebase. And by reading the following guidelines, we
are hoping you get al the information you need to write beautiful and working
code.

## TL;DR

```
git config core.hooksPath bin/git-hooks
git config commit.template .git-commit-template
```

## Code structure

This code is hosted on gitlab, please see the README.md for the exact
location of this specific codebase. We use `git` as our repository software.

## Code quality

Along with readable code, we like extensible documentation, unit tests and
a solid understanding of the location of our code. We use the following tools
to make sure we do not forget something:

    isort   # Makes sure all those "import" lines are sorted
    black   # Makes sure we use the proper indentation, line width, etc.

In short:

* Use spaces instead of tabs
* Make sure your maximum line width does not exceed 79 characters
* Write a test for every function
* Write your documentation

Or, just make it easey for yourself, and use the proper tools for the job:

### Git setup

We provided some git-hooks, which can check your code for proper indentation
and style.

On the commandline, run:

```
git config core.hooksPath bin/git-hooks
```

### Editor setup

* Plase use the editoconfiguration from .editorconfig, instructions:
  https://editorconfig.org/
* For VIM: https://realpython.com/vim-and-python-a-match-made-in-heaven/
* On save, run bin/TODO

### Sublime

**Package Control: Install packages**

**sublack**

```
{
  "black_line_length": 79,
  "black_on_save": true,
 }
```

**Python Flake8 Lint** 
```
{

  "ignore": ["E203", "E266", "E501", "W503"],
  "complexity": 10,
}
```


## Committing code

After you followed our code quality guidelines above, you are ready for a code
review from someone else. We assume you already forked the project to your
own private repository.

### Commit message

Please make sure your commit message is correct. Use a propper shortlog and be
verbose in your long log for what has changed and why.
Make sure we know whether it is a feature or a bugfix, or simply, follow the
given commit message template `.git-commit-template`. You can set it up as a
default by running:

```
git config commit.template .git-commit-template
```

### Merge request

Now that you have a solid commmit message describing the change you've made
to the software, it is time to create a merge request, so other people can
review your changes. Use the "Merge request" button from within gitlab to do
just that.

People will think something about your implementation, but they will allways
make sure you know that to do with it. The rules below describe the checks
a developer does when reviewing your code.

#### Code review rules

* Every function contains valid documentation
* Every function has a sunny day unit test
