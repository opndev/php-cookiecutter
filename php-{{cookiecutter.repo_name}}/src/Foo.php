<?php
namespace {{cookiecutter.namespace}};

class Foo {

    private $thing = "I'm {{cookiecutter.namespace}}/Foo";

    public function say() {
        print $this->thing;
    }
}

?>
