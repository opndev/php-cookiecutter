<?php

use PHPUnit\Framework\TestCase;
use {{cookiecutter.namespace}}\Foo;

class FooTest extends TestCase {
    public function testInstantiation() {

        $foo = new Foo();
        $this->assertInstanceOf('{{cookiecutter.namespace}}\Foo', $foo);
    }
}

?>
